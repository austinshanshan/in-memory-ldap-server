package sshan.ldap.inmemory.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.google.gson.Gson;
import sshan.ldap.inmemory.domain.Server;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.net.URI;
import java.net.URL;


public class ConfigurationLoader {

    private static Log logger = LogFactory.getLog(ConfigurationLoader.class);

    public static Server load(final String configJsonFile) {
        logger.debug(">>>ConfigurationLoader.loadConfiguration()");

        URL configUrl = ConfigurationLoader.class.getClassLoader().getResource(configJsonFile);

        BufferedReader br = null;
        Gson gson = new Gson();
        try {
            br = new BufferedReader(new FileReader(new File(configUrl.toURI())));
        } catch (Exception e) {
            logger.error("Unable to read config file");
        }

        Server serverObj = gson.fromJson(br, Server.class);
        System.out.println("Password: " + serverObj.getPassword());
        System.out.println("Schema: " + serverObj.getSchema().getName());

        return serverObj;
    }

}
