package sshan.ldap.inmemory.domain;

import java.util.List;

public class Root {
    private String objectDn;
    private List<String> objectClasses;

    public String getObjectDn() {
        return objectDn;
    }

    public void setObjectDn(String objectDn) {
        this.objectDn = objectDn;
    }

    public java.util.List<String> getObjectClasses() {
        return objectClasses;
    }

    public void setObjectClasses(List<String> objectClasses) {
        this.objectClasses = objectClasses;
    }

}
