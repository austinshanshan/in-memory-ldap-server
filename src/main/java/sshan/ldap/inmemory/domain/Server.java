package sshan.ldap.inmemory.domain;

import java.util.List;


public class Server {

    private String bindDn;
    private String password;
    private Schema schema;
    private Root root;
    private List<Entry> entries;
    private List<Listener> listeners;
    private List<Ldif> ldifs;

    public String getBindDn() {
        return bindDn;
    }

    public void setBindDn(String bindDn) {
        this.bindDn = bindDn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Schema getSchema() {
        return schema;
    }

    public void setSchema(Schema schema) {
        this.schema = schema;
    }

    public Root getRoot() {
        return root;
    }

    public void setRoot(Root root) {
        this.root = root;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

    public List<Listener> getListeners() {
        return listeners;
    }

    public void setListeners(List<Listener> listeners) {
        this.listeners = listeners;
    }

    public List<Ldif> getLdifs() {
        return ldifs;
    }

    public void setLdifs(List<Ldif> ldifs) {
        this.ldifs = ldifs;
    }

}
