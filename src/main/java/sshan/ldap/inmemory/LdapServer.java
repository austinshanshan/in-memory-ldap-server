package sshan.ldap.inmemory;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFChangeRecord;
import com.unboundid.ldif.LDIFReader;
import org.springframework.util.StringUtils;
import sshan.ldap.inmemory.domain.Entry;
import sshan.ldap.inmemory.domain.Ldif;
import sshan.ldap.inmemory.domain.Listener;
import sshan.ldap.inmemory.domain.Server;
import sshan.ldap.inmemory.utils.ConfigurationLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;

public class LdapServer {

    // Private constructor suppresses
    // default public constructor
    private LdapServer() {};

    private static volatile LdapServerImpl instance = null;

    public static LdapServerImpl getInstance() {
        if (instance == null) {
            synchronized (LdapServer.class) {
                if (instance == null) {
                    instance = new LdapServerImpl();
                }
            }
        }
        return instance;
    }

}