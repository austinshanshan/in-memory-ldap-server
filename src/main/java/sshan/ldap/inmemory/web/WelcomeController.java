package sshan.ldap.inmemory.web;

import sshan.ldap.inmemory.service.WelcomeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WelcomeController {

    @Autowired
    private WelcomeService welcomeService;

    @GetMapping("/")
    @ResponseBody
    public String helloWorld() {
        return this.welcomeService.getWelcomeMessage();
    }

}