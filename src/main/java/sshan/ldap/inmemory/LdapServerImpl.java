package sshan.ldap.inmemory;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFChangeRecord;
import com.unboundid.ldif.LDIFException;
import com.unboundid.ldif.LDIFReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;
import sshan.ldap.inmemory.domain.Entry;
import sshan.ldap.inmemory.domain.Ldif;
import sshan.ldap.inmemory.domain.Listener;
import sshan.ldap.inmemory.domain.Server;
import sshan.ldap.inmemory.utils.ConfigurationLoader;

import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class LdapServerImpl {

    private static Log logger = LogFactory.getLog(LdapServerImpl.class);
    private InMemoryDirectoryServer server;
    private Server configuration;
    private static final int LOCK_TIMEOUT = 60;
    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;
    private static final String configJsonFile = "ldap-in-memory-config.json";
    private static final String LOCK_TIMEOUT_MSG = "Unable to obtain lock due to timeout after " + LOCK_TIMEOUT + " " + TIME_UNIT.toString();
    private final AtomicBoolean isStarted = new AtomicBoolean(Boolean.FALSE);
    private static final String SERVER_ALREADY_STARTED = "The LDAP server is already started.";

    private final ReentrantLock serverStateLock = new ReentrantLock();

    public LdapServerImpl() {

    }

    public void start() {
        boolean hasLock = false;
        try {
            hasLock = serverStateLock.tryLock(LdapServerImpl.LOCK_TIMEOUT, LdapServerImpl.TIME_UNIT);
            if (hasLock) {
                String configFile = LdapServerImpl.configJsonFile;
                configuration = ConfigurationLoader.load(configFile);
            } else {
                throw new IllegalStateException(LdapServerImpl.LOCK_TIMEOUT_MSG);
            }
            doStart();
            this.isStarted.set(Boolean.TRUE);

        } catch (InterruptedException e) {
            logger.error("");
        } finally {
            if(hasLock) {
                serverStateLock.unlock();
            }
        }
    }

    private void doStart() {
        if (isStarted.get()) {
            throw new IllegalStateException(LdapServerImpl.SERVER_ALREADY_STARTED);
        }
        logger.info("Starting up in-Memory Ldap Server.");
        configureAndStartServer();
    }


    public void configureAndStartServer() {
        try {
            System.out.println(configuration.getPassword());
            Schema schema = null;
            if (configuration.getSchema() == null || StringUtils.isEmpty(configuration.getSchema().getName())) {
                schema = Schema.getDefaultStandardSchema();
            } else {
                final String schemaName = configuration.getSchema().getName();

                File schemaFile = new File(schemaName);
                schema = Schema.getSchema(schemaFile);
            }

            final String rootObjectDN = configuration.getRoot().getObjectDn();

            final InMemoryDirectoryServerConfig dsConfig = new InMemoryDirectoryServerConfig(new DN(rootObjectDN));
            dsConfig.setSchema(schema);

            System.out.println(configuration.getLdifs().get(0).getName());
            addListenersToConfig(configuration, dsConfig);
            dsConfig.addAdditionalBindCredentials(configuration.getBindDn(), configuration.getPassword());

            server = new InMemoryDirectoryServer(dsConfig);
            server.startListening();
            logger.info(server);
            logger.info(server.getListenAddress());
            logger.info(server.getBaseDNs());
            logger.info(server.getListenPort());
            addRootEntry(configuration);
            addEntries(configuration);
            addLDIFFiles(configuration);


        } catch (Exception e) {
            logger.info(e);
        }
    }


    public void stop() {
        server.shutDown(true);
    }


    public void addLDIFFiles(Server configuration) throws IOException, LDIFException, LDAPException {
        /*Add LDIF files*/
        int ldifLoadCount = 0;
        for (Ldif ldif : configuration.getLdifs()) {
            ldifLoadCount++;
            InputStream resourceAsStream = null;
            try {
                resourceAsStream = new FileInputStream(ldif.getName());

                if (resourceAsStream == null) {
                    throw new FileNotFoundException("Should be able to load: " + ldif.getName());
                }
                LDIFReader r = new LDIFReader(resourceAsStream);
                LDIFChangeRecord readEntry = null;
                int entryCount = 0;
                while ((readEntry = r.readChangeRecord()) != null) {
                    entryCount++;
                    readEntry.processChange(server);
                }
            } finally {
                if (resourceAsStream != null) {
                    resourceAsStream.close();
                }
            }
        }
    }

    public void addEntries(Server configuration) throws LDAPException {

        if (configuration.getEntries() != null && configuration.getEntries().size() > 0) {
            for (Entry configEntry : configuration.getEntries()) {
                logger.info("   creating entry.");
                final String attributeName = "objectClass";
                final Collection<String> attributeValues = new ArrayList<String>();
                for (String name : configEntry.getObjectClasses()) {
                    attributeValues.add(name);
                }
                com.unboundid.ldap.sdk.Entry en = new com.unboundid.ldap.sdk.Entry(new DN(configEntry.getObjectDn()));
                en.addAttribute(attributeName, attributeValues);
                server.add(en);
                logger.info("   Added entry 1: {} ");
            }
        }

        logger.info(server.getEntry("ou=rcEnvironment,dc=slidev,dc=org"));
    }


    public void addRootEntry(Server configuration) throws LDAPException {
        SearchResultEntry entry = server.getEntry(configuration.getRoot().getObjectDn());
        if (entry == null) {
            logger.info("   Root entry not found, create it.");
            final String attributeName = "objectClass";
            final Collection<String> attributeValues = new ArrayList<String>();
            for (String name : configuration.getRoot().getObjectClasses()) {
                attributeValues.add(name);

            }
            com.unboundid.ldap.sdk.Entry rootEntry = new com.unboundid.ldap.sdk.Entry(new DN(configuration.getRoot().getObjectDn()));
            rootEntry.addAttribute(attributeName, attributeValues);
            server.add(rootEntry);
        }
        entry = server.getEntry(configuration.getRoot().getObjectDn());

        logger.info("   Added root entry: {}");
        logger.info(entry.getDN());
    }


    public void addListenersToConfig(Server configuration, InMemoryDirectoryServerConfig dsConfig) throws UnknownHostException, LDAPException {
        Collection<InMemoryListenerConfig> listenerConfigs = new ArrayList<InMemoryListenerConfig>();

        if (configuration.getListeners() != null) {
            for (Listener listener : configuration.getListeners()) {
                InetAddress listenAddress = null;
                if (!StringUtils.isEmpty(listener.getAddress())) {
                    listenAddress = InetAddress.getByName(listener.getAddress());
                }
                int listenPort = Integer.parseInt(listener.getPort());
                javax.net.ServerSocketFactory serverSocketFactory = null;  //if null uses the JVM default socket factory.
                javax.net.SocketFactory clientSocketFactory = null;  //if null uses the JVM default socket factory.
                javax.net.ssl.SSLSocketFactory startTLSSocketFactory = null; //StartTLS is not supported on this listener.
                InMemoryListenerConfig listenerConfig = new InMemoryListenerConfig(listener.getName(), listenAddress, listenPort, serverSocketFactory, clientSocketFactory, startTLSSocketFactory);
                listenerConfigs.add(listenerConfig);
            }
        }

        dsConfig.setListenerConfigs(listenerConfigs);
    }
}
