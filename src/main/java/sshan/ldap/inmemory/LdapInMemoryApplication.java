package sshan.ldap.inmemory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;

import com.unboundid.ldap.sdk.DN;

import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.schema.Schema;
import com.unboundid.ldif.LDIFChangeRecord;
import com.unboundid.ldif.LDIFReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;
import sshan.ldap.inmemory.domain.Ldif;
import sshan.ldap.inmemory.domain.Server;
import sshan.ldap.inmemory.domain.Listener;
import sshan.ldap.inmemory.domain.Entry;

import sshan.ldap.inmemory.utils.ConfigurationLoader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.InetAddress;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;


@SpringBootApplication
public class LdapInMemoryApplication {

    private static Log logger = LogFactory.getLog(LdapInMemoryApplication.class);


    @Bean
    protected ServletContextListener listener() {
        return new ServletContextListener() {

            @Override
            public void contextInitialized(ServletContextEvent sce) {

                LdapServer.getInstance().start();
                logger.info("ServletContext initialized");
            }

            @Override
            public void contextDestroyed(ServletContextEvent sce) {
                LdapServer.getInstance().stop();
                logger.info("ServletContext destroyed");
            }

        };
    }

    public static void main(String[] args) {
        SpringApplication.run(LdapInMemoryApplication.class, args);
    }

}