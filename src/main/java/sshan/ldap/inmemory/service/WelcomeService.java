package sshan.ldap.inmemory.service;

import org.springframework.stereotype.Component;

@Component
public class WelcomeService {

    public String getWelcomeMessage() {
        return "Welcome to LDAP Server!";
    }

}