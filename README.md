# In Memory LDAP Server:


### Tools used to build in-memory-ldap server:
* SpringBoot
* UnboundedID Java SDK
* GSon


### Configure the in-memory-ldap server:
* The configuration information of in-memory-ldap server is as below (ldap-in-memory-config.json):
```json
{
    "bindDn": "cn=Admin,dc=sshan,dc=org",
    "password": "test1234",
    "schema": { "name": "./src/main/resources/ldif/ldap-schema.ldif" },
    "root": {
        "objectDn": "dc=sshan,dc=org",
        "objectClasses": [
                "domain",
                "top"
            ]
    },
    "entries": [
        {
            "objectDn": "ou=ssEnvironment,dc=sshan,dc=org",
            "objectClasses": ["organizationalUnit"]
        }
    ],
    "listeners": [
        {
            "name": "LDAP",
            "port": "10389"
        },
        {
            "name": "LDAP2",
            "port": "10386"
        }
    ],
    "ldifs": [
        { "name": "./src/main/resources/ldif/2014_01_16_ldap_export.ldif.modified" }
    ]
}
```

### The schema of in-memory-ldap server(ldap-schema.ldif) has 4 components:
    * ldapSyntaxes
    * ObjectClass
    * Attribute
    * MatchingRules
    

### To load your own DIT tree,  replace "2014_01_16_ldap_export.ldif.modified" with your LDIF file.



### Steps to start the in-memory-ldap server:
1. mvn package
2. mvn spring-boot:run

### Install ldapsearch command:
1. brew install openldap
2. ldapsearch --version

### Sample Commands to play with the in-memory-ldap server:
* ldapsearch -x -H "ldap://localhost:10389" -LLL "namingContexts"
* ldapsearch -x -H "ldap://localhost:10386" -b '' -s base '(objectclass=*)' namingContexts

* ldapsearch -x -H "ldap://localhost:10386" -b "dc=slidev, dc=org" "cn=Linda Kim" uid
* ldapsearch -x -H "ldap://localhost:10386" -b "dc=slidev, dc=org" -s sub "objectclass=*"


Authenticate Linda Kim:
* ldapsearch -x -H "ldap://localhost:10386" -b "dc=slidev, dc=org" -D "cn=Linda Kim,ou=people,ou=IL-DAYBREAK,dc=slidev,dc=org" -w 33baobao 
* ldapsearch -x -H "ldap://localhost:10386" -b "dc=slidev, dc=org" -D "cn=admin,dc=slidev,dc=org" -w test1234

change password for Linda Kim:
* ldappasswd -H "ldap://localhost:10386" -x -D "cn=admin,dc=slidev,dc=org" -W -S  "cn=Linda Kim,ou=people,ou=IL-DAYBREAK,dc=slidev,dc=org"
